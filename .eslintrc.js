module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2021: true
	},
	extends: ["eslint:recommended", "plugin:react/recommended", "prettier", "plugin:react/jsx-runtime"],
	parserOptions: {
		sourceType: "module",
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: "latest"
	},
	plugins: ["react", "prefer-arrow"],
	rules: {
		"prefer-arrow/prefer-arrow-functions": [
			"warn",
			{
				disallowPrototype: true,
				singleReturnOnly: false,
				classPropertiesAllowed: true,
				allowStandaloneDeclarations: false
			}
		],
		indent: ["error", "tab"],
		quotes: ["error", "double"],
		semi: ["error", "always"],
		"no-var": ["error"],
		"arrow-body-style": ["error", "as-needed"],
		complexity: "off",
		"constructor-super": "error",
		curly: ["error", "multi", "consistent"],
		"default-case": "error",
		"dot-notation": "error",
		"eol-last": "error",
		eqeqeq: ["error", "always"],
		"guard-for-in": "error",
		"id-blacklist": "off",
		"no-bitwise": "error",
		"no-caller": "error",
		"no-cond-assign": "error",
		"no-empty": "error",
		"no-fallthrough": "error",
		"no-new-wrappers": "error",
		"no-redeclare": "error",
		"no-throw-literal": "error",
		"no-trailing-spaces": "off",
		"no-undef-init": "error",
		"no-underscore-dangle": "error",
		"no-unsafe-finally": "error",
		"no-unused-expressions": "error",
		"no-unused-labels": "error",
		"object-shorthand": "error",
		"one-var": ["error", "never"],
		"prefer-arrow-callback": "error",
		radix: ["error", "as-needed"],
		"space-before-function-paren": [
			"error",
			{
				anonymous: "never",
				asyncArrow: "always",
				named: "never"
			}
		],
		"react/prop-types": "off",
		"spaced-comment": ["error", "always"],
		"use-isnan": "error",
		"valid-typeof": "off"
	},
	settings: {
		react: {
			version: "detect"
		}
	}
};
