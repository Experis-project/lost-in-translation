# Lost in translation
Lost in translation is a website where you can translate English letters, words and sentences to American sign language. 

## Description
The app is created with React and a private REST API. To use the site, user needs to enter their name which works as a login. Name gets saved along with search history to the database and can be accessed by reusing the same name later. On a profile page logged in user can see their last 10 searches, clear the search history and log out. 

The app can be used here: https://tk-lost-in-translation.herokuapp.com/

## Features
-   Create a user account with just a username
-   Login/Logout
-   Translate English to American sign language
-   See your last 10 translations
-   Delete translation history

### Libraries used
-   React
-   Bootstrap
-   React-Bootstrap

## Install:
```
-   To run the code on local machine you will need access to the API (.env file) or create your own backend. 
-   You will also need Node.js: https://nodejs.org/en/download/ and git: https://git-scm.com/downloads
-   In your console navigate to the folder where you want the project and run the following commands:
    -   git clone https://gitlab.com/Experis-project/lost-in-translation.git
    -   cd lost-in-translation
    -   npm install
-   For API access, create ".env" file in the root folder of the project, add the following lines and paste correct API key and url inside the quotation marks:
    {    
        REACT_APP_API_KEY = "key"
        REACT_APP_API_URL = "base url"
    }    
```

## Usage
-   After installation run "npm start" in the command line and you can use the app in your browser at the following address: http://localhost:3000
-   Enter a username to create account or to log back in.
-   After being redirected enter english words or sentences to be translated
-   Open profile page from your username in the top right corner to see or delete search history and to logout.

## Contributors
- Maxim Anikin https://gitlab.com/MaximusAnakin
- Tuomo Kuusisto https://gitlab.com/pampula


## License
[MIT](https://choosealicense.com/licenses/mit/)
