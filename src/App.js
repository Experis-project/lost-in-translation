import Header from "./Header/header";
import { BrowserRouter } from "react-router-dom";
import Router from "./Router";

const App = () => (
	<BrowserRouter>
		<Header />
		<Router />
	</BrowserRouter>
);

export default App;
