import { Routes, Route } from "react-router-dom";
import Login from "./Login/Login";
import Profile from "./Profile/Profile";
import Translation from "./Translation/Translation";

const Router = () => (
	<Routes>
		<Route path="/" element={<Login />} />
		<Route path="/translation" element={<Translation />} />
		<Route path="/profile" element={<Profile />} />
		<Route path="*" element={<>Page not found</>} />
	</Routes>
);
export default Router;
