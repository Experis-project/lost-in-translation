import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import {useUser} from "../Context/UserContext";
import person from "../Images/person.png";
import shakaIcon from "../Images/shakaIcon.png";


const Header = () => {
	const { user } = useUser();

	return (
		<Navbar bg="dark" variant="dark">
			<Container>
				<Link 
					
					to={"/"} 
					style={{color: "white", textDecoration: "none"}}>
					<img src={shakaIcon} style={{
						width: "20px",
						margin: "0px 5px 5px 0px"							
					}}/>
						LOST IN TRANSLATION
				</Link>
				{user && 
					<div className="justify-content-end">
						<Link 
							to={"/profile"} 
							style={{color: "white", textDecoration: "none"}}>
							{user.username}
							<img src={person} alt="person symbol"/>
						</Link>
					</div>
				}
			</Container>
		</Navbar>
	);};

export default Header;
