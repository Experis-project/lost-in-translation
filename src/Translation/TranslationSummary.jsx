import TranslatedSign from "./TranslatedSign";

const TranslationSummary = ({arrayWord}) => {

	// Remove unsupported characters and symbols
	const regex = /[a-zA-Z]/;
	arrayWord = arrayWord.filter(char => char.match(regex));

	return (
		<div style={{display: "flex", justifyContent: "center", flexDirection: "row", alignItems: "center"}}>
			{arrayWord.map((letter,index) => (
				<TranslatedSign key={index + " " + letter} letter={letter}></TranslatedSign>
			))}
		</div>
	);
};

export default TranslationSummary;
