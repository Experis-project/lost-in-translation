import {useForm} from "react-hook-form";
import Button from "react-bootstrap/Button";

const formStyle = {
	display: "flex", 
	justifyContent: "center",  
	margin: "50px"
};

const inputStyle = {
	width: "300px", 
	paddingRight:"100px",
	borderRadius: "5px", 
	border: "1px solid black"
};

const buttonStyle = {
	marginLeft: "-99px", 
	width:"100px"
};

const TranslationForm = ({onTranslate, setArrayWord}) => {
	const { register, handleSubmit} = useForm();

	const onSubmit = ({translationText}) => {
		setArrayWord(translationText.toLowerCase().split(""));
		{
			/* Text is forced to lowercase to make sure the entries correspond with the image names.
				Text is then split into an array so that every character can be handled separately. */
		}

		onTranslate(translationText);
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<fieldset>
				<div style={formStyle}>
					<input 
						style={inputStyle} 
						type={"text"}
						placeholder="Translate from English"
						{...register("translationText")}/>
					<Button 
						style={buttonStyle} 
						type={"submit"}>
							Translate
					</Button>
				</div>
			</fieldset>
		</form>
	);
};

export default TranslationForm;
