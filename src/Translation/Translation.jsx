import TranslationForm from "./TranslationForm";
import {useState} from "react";
import TranslationSummary from "./TranslationSummary";
import withAuth from "../HOC/withAuth";
import {addTranslation} from "../Api/translations";
import {useUser} from "../Context/UserContext";
import {storageSave} from "../Utils/storage";
import {STORAGE_KEY_USER} from "../Const/storageKeys";

const titleStyle = {
	display: "flex", 
	justifyContent: "center", 
	marginTop: "50px"
};

const Translation = () => {

	const [translationText, setTranslationText ] = useState(null);
	const [arrayWord, setArrayWord] = useState([]);
	const {user, setUser } = useUser();

	// Async method because the search will be submitted to database.
	// Search result will be shown before the await method because the images are stored locally and
	// the result can therefore be shown independently.
	const handleSubmitClicked = async translation =>{
		if(translation.length < 1){
			alert("Please write at least one character");
			return;
		}

		setTranslationText(translation);
		const [error, updatedUser] = await addTranslation(user, translation);
		if(error !== null)
			return;

		// Keep UI state and server in sync
		storageSave(STORAGE_KEY_USER, updatedUser);
		// Update context state
		setUser(updatedUser);
	};

	return (
		<>
			<div style={titleStyle}>
				<h1>Translate to sign language</h1>
			</div>
			<section id={"translation-result"}>
				<TranslationForm onTranslate= {handleSubmitClicked} setArrayWord={setArrayWord} />
			</section>
			{ translationText && <TranslationSummary arrayWord = {arrayWord}/>  }
			{ /* Show result only when translationText is set */}

		</>
	);
};

export default withAuth(Translation);
