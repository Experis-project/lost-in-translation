// The images for the signs are stored with the corresponding letter as the filename, e.g. "a.png"
const TranslatedSign = ({letter}) => (
	<img src={`img/individual_signs/${letter}.png`} alt={letter} width='60' />
);

export default TranslatedSign;
