import {useUser} from "../Context/UserContext";
import {Navigate} from "react-router-dom";

// eslint-disable-next-line react/display-name
const withAuth = Component => props => {
	const {user} = useUser();
	if(user !== null)
		return <Component {...props}/>; // Forward any props that were received
	else
		return <Navigate to={"/"}/>;
};

export default withAuth;
