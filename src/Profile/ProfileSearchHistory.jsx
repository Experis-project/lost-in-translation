import ProfileSearchHistoryItem from "./ProfileSearchHistoryItem";

const ProfileSearchHistory = ({translations}) => {
// Populates profile page with search history. Shows a message if search history is empty.

	// Map messages so that each element has a unique key
	const searchHistory = translations.map(
		(translation, index) => <ProfileSearchHistoryItem key={index + "-" + translation} translation={translation}/>);

	return(
		<section>
			{translations.length === 0 &&
				<p style={{textAlign: "center"}}>
					Your search history is empty.</p>}
			<ul className="list-group list-group-flush" style={{borderRadius: "10px", height: "415px", overflow: "auto"}}>
				{searchHistory.reverse().slice(0,10)}
				{/* Shows the 10 most recent translations (first index in the array is the oldest message) */}
			</ul>
		</section>
	);
};

export default ProfileSearchHistory;
