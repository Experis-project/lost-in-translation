import ProfileSearchHistory from "./ProfileSearchHistory";
import withAuth from "../HOC/withAuth";
import ProfileHeader from "./ProfileHeader";
import ProfileActions from "./ProfileActions";
import {useUser} from "../Context/UserContext";

const Profile = () => {
	const { user, } = useUser();

	return(
		<div style={{display: "flex", justifyContent: "center", margin: "30px"}}>
			<div style={{
				marginRight: "30px", 
				backgroundColor: "lightGrey", 
				width: "300px",
				height: "200px", 
				borderRadius: "10px", 
				padding: "30px",
				boxShadow: "5px 5px 5px grey"
			}}>
				<ProfileHeader username={user.username} />
				<ProfileActions />
			</div>
			<div style={{
				backgroundColor: "lightGrey", 
				width: "500px",
				height: "470px", 
				borderRadius: "10px", 
				padding: "30px",
				boxShadow: "5px 5px 5px grey"
			}}><ProfileSearchHistory translations={user.translations}/>
			</div>
			
		</div>
	);
};

export default withAuth (Profile);
