const ProfileSearchHistoryItem = ({translation}) => <li style={{overflowWrap: "breakWord"}} className="list-group-item">{translation}</li>;

export  default  ProfileSearchHistoryItem;
