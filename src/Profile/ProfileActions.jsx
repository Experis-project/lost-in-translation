import {storageDelete, storageSave} from "../Utils/storage";
import {useUser} from "../Context/UserContext";
import {STORAGE_KEY_USER} from "../Const/storageKeys";
import {clearTranslationHistory} from "../Api/translations";

const ProfileActions = () => {
	const { user, setUser } = useUser();

	const handleLogoutClick = () => {
		if(!window.confirm("Are you sure you want to log out?"))
			return;

		// Delete user from local storage
		storageDelete(STORAGE_KEY_USER);
		setUser(null);
	};

	const handleClearHistoryClick = async () => {
		if(!window.confirm("Are you sure you want to clear the search history?"))
			return;

		const [clearError] = await clearTranslationHistory(user.id);
		if(clearError !== null)
			throw new Error("Could not clear translation history!");

		const updatedUser = {
			...user,
			translations:[]
		};

		storageSave(STORAGE_KEY_USER, updatedUser);
		setUser(updatedUser);
	};

	return (
		<>
			<div className={"text-center"} style={{
				justifyContent: "center",
				flexDirection: "column",
				padding: "20px 0 0 0",
				display: "grid",
				gridAutoFlow: "column",
				gridColumnGap: "20px"
			}}>
				<button onClick={handleClearHistoryClick} type="button" className="btn btn-danger btn-sm">
					Clear history
				</button>
				<button onClick={handleLogoutClick} type="button" className="btn btn-primary btn-sm btn-block">
					Log out
				</button>
			</div>
		</>
	);
};

export default ProfileActions;
