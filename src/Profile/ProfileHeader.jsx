const ProfileHeader = ({username}) => (
	<h2 style={{textAlign: "center"}}>
		User: {username}
	</h2>
);

export default ProfileHeader;
