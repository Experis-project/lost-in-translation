import UserProvider from "./UserContext";

const AppContext = ({children}) => (
	<UserProvider>
		{children}
	</UserProvider>
);

export default AppContext;
