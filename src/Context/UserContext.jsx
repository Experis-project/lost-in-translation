import {createContext, useContext, useState} from "react";
import {storageRead} from "../Utils/storage";
import {STORAGE_KEY_USER} from "../Const/storageKeys";

// Context -> exposing state
const UserContext = createContext();

export const useUser = () => useContext(UserContext); // Returns object { user, setUser }

// Provider -> managing state
const UserProvider = ({children}) => {

	const [user, setUser] = useState(storageRead(STORAGE_KEY_USER));

	const state = {
		user,
		setUser
	};

	return(
		<UserContext.Provider value={state}>
			{children}
		</UserContext.Provider>
	);
};

export default UserProvider;
