import {createHeaders} from "./index";
// eslint-disable-next-line no-undef
const apiUrl = process.env.REACT_APP_API_URL;

// POST - create new record
// PATCH - Update parts of record
// GET - Read records
// Delete - Removes a record
// PUT - Replaces entire record (similar to PATCH)

// Adds a new translation to users translation history (PATCH)
export const addTranslation = async (user, newTranslation) => {
	try{
		const response = await fetch(`${apiUrl}/${user.id}`, {
			method: "PATCH",
			headers: createHeaders(),
			body: JSON.stringify({
				translations: [...user.translations, newTranslation]
			}) // JS object needs to be in string format
		});

		if(!response.ok)
			throw new Error("Could not update translation history.");

		const result = await response.json();
		return [null, result];
	} catch(error){
		return [error.messages, null];
	}
};

// Deletes user's whole translation history (PATCH)
export const clearTranslationHistory = async (userId) => {
	try{
		const response = await fetch(`${apiUrl}/${userId}`, {
			method: "PATCH",
			headers: createHeaders(),
			body: JSON.stringify({
				translations: []
			})
		});
		if(!response.ok)
			throw new Error("Could not clear history.");
        
		const result = await response.json();
		return [null, result];
	} catch(error){
		return [error.messages, null];
	}
};
