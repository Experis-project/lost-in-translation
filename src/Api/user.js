import {createHeaders} from "./index";
// eslint-disable-next-line no-undef
const apiUrl = process.env.REACT_APP_API_URL;

const checkForUser = async (username) => {
	try{
		const response = await fetch(`${apiUrl}?username=${username}`);
		if(!response.ok)
			throw new Error("Could not complete the request.");

		const data = await response.json();
		return [null,data];
	}
	catch (error){
		return[error.message, []];
	}
};

const createUser = async (username) => {
	try{
		const response = await fetch(apiUrl, {
			method: "POST", // Create a resource
			headers: createHeaders(),
			body: JSON.stringify({
				username,
				translations: []
			})
		});
		
		if(!response.ok)
			throw new Error("Could not create a user with username " + username);

		const data = await response.json();
		return [null,data];
	}
	catch (error){
		return[error.message, []];
	}
};

export const loginUser = async (username) => {
	const [ checkError, user ] = await checkForUser(username);

	if(checkError !== null)
		return [checkError, null];
	
	if(user.length > 0)
		return [null, user.pop()];
	

	return await createUser(username);
};

export const findUserById = async (userId) => {
	try{
		const response = await fetch(`${apiUrl}/${userId}`);

		if(!response.ok)
			throw new Error("Could not fetch user " + userId);

		const user = await response.json();
		return [null, user];
	}
	catch(error){
		return [error.message, null];
	}
};
