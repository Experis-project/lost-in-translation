import {useForm} from "react-hook-form";
import {loginUser} from "../Api/user";
import {useState, useEffect} from "react";
import {storageSave} from "../Utils/storage";
import {useNavigate} from "react-router-dom";
import {useUser} from "../Context/UserContext";
import {STORAGE_KEY_USER} from "../Const/storageKeys";
import Button from "react-bootstrap/Button";

// Requirements for username.
const usernameConfig = {
	required: true,
	minLength: 3,
	maxLength: 20
};

const formStyle = {
	margin: "50px", 
	display: "flex", 
	flexDirection: "row"
};

const inputStyle = {
	width: "300px", 
	paddingRight:"100px", 
	borderRadius: "5px", 
	border: "1px solid black"
};

const buttonStyle = {
	marginLeft: "-99px", 
	width:"100px" 
};

const LoginForm = () => {
	// Deconstructed useForm, no page reload
	const { register, handleSubmit, formState: {errors}} = useForm();
	const { user, setUser } = useUser();
	const navigate = useNavigate();

	// Local state
	const [loading, setLoading] = useState(false);
	const [apiError, setApiError] = useState(null);

	// Side effects
	useEffect(() => {
		if(user !== null)
			navigate("/translation");
		
	}, [user, navigate]);

	// Event handlers
	const onSubmit = async ({username}) => {
		setLoading(true); // Not working as expected?
		const [error, userResponse ] = await loginUser(username);
		if(error !== null)
			setApiError(error);

		if(userResponse !== null)
			storageSave(STORAGE_KEY_USER, userResponse);

		setUser(userResponse); // User set to local storage
		setLoading(false); // Not working as expected?
	};

	// Render functions. Checks that username meets the minimum requirements.
	const errorMessage= (() =>{
		if(!errors.username)
			return null;

		if(errors.username.type === "required")
			return <span>Username is required.</span>;

		if(errors.username.type === "minLength")
			return <span>Name minimum length is 3.</span>;

		if(errors.username.type === "maxLength")
			return <span>Name max length is 20.</span>;

	})();

	return(
		<>
			<form onSubmit={handleSubmit(onSubmit)}>
				<fieldset>
					<div style={formStyle}>
						<input
							type="text"
							placeholder="Enter your name"
							{...register("username", usernameConfig)}
							style={inputStyle}
						/>
						<Button 
							variant="primary" 
							style={buttonStyle} 
							type={"submit"} 
							disabled={loading}>
								Continue
						</Button>
					</div>
				</fieldset>
			</form>
			{ errorMessage }
			{loading && <p>Logging in...</p>}
			{apiError && <p>{apiError}</p>}
		</>
	);
};

export default LoginForm;
