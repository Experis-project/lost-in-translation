import LoginForm from "./LoginForm";
import logo from "../Images/shaka.png";
 
const mainStyle = {
	display: "flex", 
	justifyContent: "center", 
	flexDirection: "column", 
	alignItems: "center", 
	margin: "30px"
};
const titleStyle = {
	display: "flex",
	alignItems: "end"
};

const Login = () => (
	<>
		<div style={mainStyle}>
			<div style={titleStyle}>
				<img src={logo} alt="shaka sign logo" style={{width: "100px"}}/>
				<h1>Lost in translation</h1>
			</div> 			
			<LoginForm />
		</div>
	</>
)
;

export default Login;
